import React from 'react';
import './input.css';
// Input para texto

// Input plano
export function Input(props){
    return(
        <input className='input'/>
    );
}

// Input para textos
export function InputText(props){
    return(
        <input className='input' type="text" name='texto' placeholder={props.placeholder} minLength={1} maxLength={50} required/>
    );
}

// Input para contraseña
export function InputPassword(props){
    return(
        <input className='input' type="password" name='texto' placeholder={props.placeholder}  onChange={props.onchange}  minLength={8} maxLength={20} required/>
    );
}

// Input para correo electrónico
export function InputEmail(props){
    return(
        <input className='input' type="email" name='texto' placeholder={props.placeholder} onChange={props.onchange} maxLength={100} required/>
    );
}

// Input para numeros
export function InputNumber(props){
    return(
        <input className='input' type='number' name='numero' placeholder={props.placeholder} min='0' required/>
    );
}

// Input para número de contacto
export function InputPhone(props){
    return(
        <input className='input' type='tel' name='celular' placeholder={props.placeholder} pattern='[0-9]{10}' maxLength={10} required/>
    );
}

// Input para fecha
export function InputDate(props){
    return(
        <input className='input'type='date' name='fecha' required/>
    );
}

// Input para hora
export function InputTime(props){
    return(
        <input className='input'type='time' name='hora' required/>
    );
}

// Input para select de método de pago
export function InputSelectMetodoPago(props){
    return(
    <div>
        {/*<input type='text' placeholder='Método de Pago' className='select' disabled/> */}
            <select className='select' required>
                <option value='0'>--Seleccionar--</option>
                <option value='1'>Pago en Efectivo</option>
                <option value='2'>Pago con Tarjeta</option>
            </select><br/><br/>
    </div>
    );
}

// Input para select de tipo de compra
export function InputSelectTipoCompra(props){
    return(
    <div>
        {/* <input type='text' placeholder='Tipo de Compra' className='select' disabled/> */}
            <select className='select' required>
                <option value='0'>--Seleccionar--</option>
                <option value='1'>Recoger en Tortillería</option>
                <option value='2'>Entrega a Domicilio</option>
            </select><br/><br/>
    </div>
    );
}

// Input para select de producto
export function InputSelectProducto(props){
    return(
    <div>
        {/* <input type='text' placeholder='Producto' className='select' disabled/> */}
            <select className='select' required>
                <option value='0'>--Seleccionar--</option>
                <option value='1'>Masa</option>
                <option value='2'>Tortillas</option>
            </select><br/><br/>
    </div>
    );
}
