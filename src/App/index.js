/* En la parte de abajo estan los imports para agregar, antes de la function App() {} */
//import React from 'react';
//import {BrowserRouter, Route, Routes} from 'react-router-dom'; //para probar el route
//import logo from '../tortilla.png';
//import { InicioDueno } from '../InicioDueno';
//import { ListaPedido, ListaVenta } from '../Lista';
//import { ItemPedido, ItemVenta } from '../Item';

//este import permite utilizar el elemento botón
//import Boton from '../Boton';

// Imports de Font Awesome
//import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
//import { faBook, faCheckSquare, faCreditCard, faDollarSign, faHistory, faList, faMoneyBill, faPen, faRemove, faSearch, faSignIn, faSignOut, faUser } from '@fortawesome/free-solid-svg-icons'


//function App() {

  /*
  //Arreglos de datos Lista
  //Array inicial de pedidos
  const pedidosArray = [
    {
      numeropedido: 1,
      fecha: '10-12-22',
      hora: '11:15',
      status: false,
    },
    {
      numeropedido: 2,
      fecha: '11-12-22',
      hora: '11:16',
      status: true,
    },
    {
      numeropedido: 3,
      fecha: '12-12-22',
      hora: '11:17',
      status: false,
    },
    {
      numeropedido: 4,
      fecha: '13-12-22',
      hora: '11:18',
      status: false,
    },
  ]

  const ventasArray = [
    {
      numerodeventa: 1,
      fecha: '10-12-22',
      monto: 100,
    },
    {
      numerodeventa: 2,
      fecha: '11-12-22',
      monto: 200,
    },
    {
      numerodeventa: 3,
      fecha: '12-12-22',
      monto: 300,
    },
    {
      numerodeventa: 4,
      fecha: '14-12-22',
      monto: 400,
    },
  ]

  //States Lista
  const [pedidos, setPedidos] = React.useState(pedidosArray);
  const [ventas, setVentas] = React.useState(ventasArray);

  //Funciones Lista 
  //Aceptar pedido
  const onAccept = (numeropedido) => {
    const index = pedidos.findIndex(pedido => pedido.numeropedido === numeropedido);
    const newPedidos = [...pedidos];
    newPedidos[index].status = true;
    setPedidos(newPedidos);
  }

  return (
    <BrowserRouter>
      <Routes>
      <Route path = '/' element={
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        //Iconos de Font Awesome
        {/*<FontAwesomeIcon icon={faDollarSign} />
        <FontAwesomeIcon icon={faPen} />
        <FontAwesomeIcon icon={faMoneyBill} />
        <FontAwesomeIcon icon={faCreditCard} />
        <FontAwesomeIcon icon={faSearch} />
        <FontAwesomeIcon icon={faHistory} />
        <FontAwesomeIcon icon={faBook} />
        <FontAwesomeIcon icon={faList} />
        <FontAwesomeIcon icon={faSignIn} />
        <FontAwesomeIcon icon={faSignOut} />
        <FontAwesomeIcon icon={faCheckSquare} />
        <FontAwesomeIcon icon={faRemove} />
        <FontAwesomeIcon icon={faUser} /> 
    
        <p>
          TortiMax 1.0.1
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          {/* este es un ejemplo de cómo se implementa el elemento botón, 
          en color se deben usar las clases que provee boostrap por ejemplo: 
          danger, primary success... 
          //<Boton text='Este es el texto del botón' color='danger'/>
           

          Learn React
        </a>

      </header>
      //Uso de Componente Lista e Item 
      //Tabla ventas 
      <ListaVenta>
        {ventas.map(venta => (
          <ItemVenta
            primero={venta.numerodeventa}
            segundo={venta.fecha}
            tercero={venta.monto}
          />
        ))
        }
      </ListaVenta>
      //Tabla pedidos 
      <ListaPedido>
        {pedidos.map(pedido => (
          <ItemPedido
            primero={pedido.numeropedido}
            segundo={pedido.fecha}//No es necesario mandarlo en la lista de ventas
            tercero={pedido.hora}
            cuarto={pedido.status}
            onAccept={() => onAccept(pedido.numeropedido)}
          />
        ))
        }
      </ListaPedido>

    </div>
    } />
    {/*Ruta al historial }
    <Route path = '/HistorialVentas' element={
          <HistorialVentas 
          
          />
        } />


    <Route path = '/InicioDueno' />
    
    </Routes>

    
  </BrowserRouter> //prueba de route ruta raiz
  );
}

export default App;
      */

import React from "react";
import { 
  BrowserRouter,
  Route,
  Routes
} from "react-router-dom";
import { Login } from "../Login";
import { NuevoPedido } from "../NuevoPedido";
import { InicioDueno } from "../InicioDueno";
import './App.css'; /* no quitar */
import { HistorialVentas } from '../HistorialVentas';
import { HistorialPedidos } from '../HistorialPedidos';
// Creo no se ocupan estos

//import Boton from "../Boton";

function App () {
  return (
    <BrowserRouter>
      <Routes>
        
        <Route  exact path='/' element={
            <NuevoPedido />
        }/>

        <Route  exact path='/login' element={
            <Login />
        }/>

        <Route  exact path='/nuevopedido' element={
            <NuevoPedido />
        }/>

        <Route  exact path='/iniciodueno' element={
            <InicioDueno />
        }/>

        <Route  exact path='/historialventas' element={
            <HistorialVentas />
        }/>

        <Route  exact path='/historialpedidos' element={
            <HistorialPedidos />
        }/>

        <Route  exact path='*' element={
            <div>Error 404</div>
        }/>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
