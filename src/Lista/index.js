import React from "react";
import './lista.css';

function ListaPedido(props){
    return(
        <section>
            <ul className="ul">
                <li className="Item-head">
                    <p className="Item-p-head">No. de Pedido</p>
                    <p className="Item-p-head">Fecha</p>
                    <p className="Item-p-head">Hora</p>
                    <p className="Item-p-head">Status</p>
                </li>
            </ul>
            <ul className="ul">
                {props.children}
            </ul>
        </section>
    )
}

function ListaVenta(props){
    return(
        <section>
        <ul className="ul">
            <li className="Item-head">
                <p className="Item-p-head">No. de Venta</p>
                <p className="Item-p-head">Fecha</p>
                <p className="Item-p-head">Monto</p>
            </li>
        </ul>
        <ul className="ul">
            {props.children}
        </ul>
    </section>
    )
}

export {ListaPedido, ListaVenta};