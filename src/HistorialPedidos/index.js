import React from 'react';
import logo from '../iconos/tortimax96.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignOut, faHistory } from '@fortawesome/free-solid-svg-icons' // agrege la cesta y mano
import { ListaPedido } from '../Lista';
import { ItemPedido } from '../Item';
import { Link } from "react-router-dom";

function HistorialPedidos(props) {

  // Array Pedidos
  const pedidosArray = [
    {
      numeropedido: 1,
      fecha: '10-12-22',
      hora: '11:15',
      status: true,
    },
    {
      numeropedido: 2,
      fecha: '11-12-22',
      hora: '11:16',
      status: true,
    },
    {
      numeropedido: 3,
      fecha: '12-12-22',
      hora: '11:17',
      status: true,
    },
    {
      numeropedido: 4,
      fecha: '28-3-22',
      hora: '18:27',
      status: true,
    },
    {
      numeropedido: 5,
      fecha: '28-3-22',
      hora: '18:50',
      status: false,
    },
  ]

  //States Lista
  const [pedidos, setPedidos] = React.useState(pedidosArray);

  //Funciones Lista 
  //Aceptar pedido
  const onAccept = (numeropedido) => {
    const index = pedidos.findIndex(pedido => pedido.numeropedido === numeropedido);
    const newPedidos = [...pedidos];
    newPedidos[index].status = true;
    setPedidos(newPedidos);
  }


  return (
    <div className="contenedor tarjetaMovil">
      <div className="contenedorMenu">
        <img src={logo} alt="logo" className="logo" />

        <span className='botonSalir'>
          <Link to={`/`} className="btn btn-success">
            Cerrar Sesión&nbsp;
            <FontAwesomeIcon
              size="1x"
              icon={faSignOut}
            /></Link>
        </span>
        <span className='iconoSalir'>
          <Link to={`/`} className="btn btn-success">
            <FontAwesomeIcon
              size="1x"
              icon={faSignOut}
            /></Link>
        </span>
      </div>

      <div className="contenedorCuerpo">
        <h2 className='titulo'>Pedidos</h2>
        <div className="tarjeta">

          <ListaPedido>
            {
              pedidos.sort((a,b) => (a.numeropedido > b.numeropedido) ? -1 : 1)
              .map(pedido => (
                <ItemPedido
                  primero={pedido.numeropedido}
                  segundo={pedido.fecha}
                  tercero={pedido.hora}
                  cuarto={pedido.status}
                  onAccept={() => onAccept(pedido.numeropedido)}
                />
              ))
            }
          </ListaPedido>
        </div>

        <span className="botonReturn">
          <Link to={`/InicioDueno`} className="btn btn-success">
            Regresar&nbsp;
            <FontAwesomeIcon
              size="1x"
              icon={faHistory}
            /></Link>
        </span>

        <span className="iconoReturn">
          <Link to={`/InicioDueno`} className="btn btn-success">
            <FontAwesomeIcon
              size="1x"
              icon={faHistory}
            /></Link>
        </span>
      </div>
    </div>
  );
}

export { HistorialPedidos };