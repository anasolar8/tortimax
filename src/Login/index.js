import React, { useEffect, useState } from "react";
//import './login.css';
import logo from '../iconos/tortimax512.png';
import { InputEmail, InputPassword } from '../Input';
import Boton from "../Boton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRightToBracket } from "@fortawesome/free-solid-svg-icons";
//import App from "../App";
import { useNavigate } from "react-router-dom";

const usuariosArray = [
    {
        id: 1,
        email: 'prueba@gmail.com',
        pass: '12345678',
        tipo: 1

    },
    {
        id: 2,
        email: 'pruebad@gmail.com',
        pass: '12345678',
        tipo: 2

    },
]

function Login() {
    const navigate = useNavigate();
    const [email, setEmail] = useState("");
    const [pass, setPass] = useState("");
    const [usuario, setUsuario] = useState(
        {
            email: '',
            pass: '',
            tipo: 0
        }
    );

    const updateEmail = (e) => {
        setEmail(e.target.value);
    }

    const updatePass = (e) => {
        setPass(e.target.value);
        setUsuario(
            {
                email: email,
                pass: e.target.value,
                tipo: 0
            }
        )
    }

    const login = () => {
        let existe = 0;
        usuariosArray.map((us, index) => {
            if (existe !== 1) {
                if (us.email.toLowerCase() === usuario.email.toLocaleLowerCase() && us.pass.toLowerCase() === usuario.pass.toLocaleLowerCase()) {
                    console.log('correo bien');
                    console.log('pass bien');
                    setUsuario(
                        {
                            email: email,
                            pass: pass,
                            tipo: us.tipo
                        }
                    )
                    switch (us.tipo) {
                        case 1:
                            existe = 1;
                            navigate("/nuevopedido", { replace: true });
                            break;
                        case 2:
                            existe = 1;
                            navigate("/iniciodueno", { replace: true });
                            break;
                        default:
                            console.log('Carayo');
                            break;
                    }
                }
            }
            if(index+1 === usuariosArray.length && existe === 0){
                alert("Usuario no existente");
                navigate("/", { replace: true });
            }
        }
        )

    }

    useEffect(() => {
        console.log(email);
        console.log(pass);
        console.log(usuario);
    }, [email, pass, usuario])

    return (
        // Si añado className="contenedor tarjeta" toma nueva forma
        <div className="contenedor">
            <div className="fila0"></div>
            <img src={logo} className="image tarjeta" alt="logo"/>
            <form onSubmit={login}>
                <div className="contenedor-2 inputLogin">
                    <InputEmail placeholder={"Correo electrónico"} onchange={updateEmail} />
                    <InputPassword placeholder={"Contraseña"} onchange={updatePass} />
                    <div>
                        <Boton
                            text={
                                <span>
                                    Acceder
                                    <FontAwesomeIcon
                                        className="icon"
                                        size="1x"
                                        icon={faRightToBracket}
                                    />
                                </span>
                            }
                            color="success"
                        />
                    </div>
                </div>
            </form>
        </div>
    )
}

export { Login };