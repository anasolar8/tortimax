import React from 'react';
import './item.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'


function ItemPedido(props){
    return(
    <li className="Item">
        <p className={`Item-p`}>
            {props.primero}
        </p>
        {props.segundo ? 
            <p className={`Item-p`}>
            {props.segundo}
            </p>
            :
            <p className={`Item-p`}>
            ----
            </p>
        }
        {props.tercero ? 
            <p className={`Item-p`}>
            {props.tercero}
            </p>
            :
            <p className={`Item-p`}>
            ----
            </p>
            /*
            <FontAwesomeIcon 
                className={props.segundo ? `Item-fa && Item-fa--active` : `Item-fa`} 
                size="2x" 
                icon={faCheckCircle} 
                onClick={props.onAccept}
            /> 
            */
        }
        {props.cuarto ? 
            <FontAwesomeIcon 
            className={`Item-fa && Item-fa--active`} 
            size="2x" 
            icon={faCheckCircle} 
            onClick={props.onAccept}
            />
            :
            <FontAwesomeIcon 
                className={`Item-fa`} 
                size="2x" 
                icon={faCheckCircle} 
                onClick={props.onAccept}
            />   
        }
    </li>
    )
}

function ItemVenta(props){
    return(
    <li className="Item">
        <p className={`Item-p`}>
            {props.primero}
        </p>
        {props.segundo ? 
            <p className={`Item-p`}>
            {props.segundo}
            </p>
            :
            <p className={`Item-p`}>
            ----
            </p>
        }
        {props.tercero ? 
            <p className={`Item-p`}>
            ${props.tercero}
            </p>
            :
            <p className={`Item-p`}>
            $---
            </p> 
        }
    </li>
    )
}

export {ItemPedido, ItemVenta};